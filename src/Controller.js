class Controller {
    constructor(model) {
        this.model = model;
    }

    addNewLeadObj = data => this.model.insertLead(data);

    updateLeadById = data => {
        this.model.updateLeadProfile(data);
    }

    deleteLeadById = id => this.model.deleteLead(id);

    returnLeadsList = () => this.model.getLeads();

    checkAdminAuthorize = adminData => {
        const adminsArray = this.model.getAdmins();

        if(!adminData) {
            return ;
        }

        let result = this.model.findAccountInArray(adminsArray, adminData);

        !result && (result = !!result);

        return result;
    }

    checkLeadAccountExist = leadData => {
        const leadsArray = this.model.getLeads2();

        if(!leadData) {
            return ;
        }

        let result = this.model.findAccountInArray(leadsArray, leadData);

        !result && (result = !!result);

        return result;
    }

    getLeadCommentArray = leadAccountObj => this.model.getLeadCommentsFromObject(leadAccountObj);
}

module.exports = Controller;


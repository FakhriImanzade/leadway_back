const Server = require('./Server')

function init () {
    const server = new Server(process.env.PORT || 3000);

    server.start();
}

init();
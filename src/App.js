const express = require('express');
const path = require('path');
const { response } = require('express');

class App{
    constructor(controller) {
        this.app = express();
        this.app.use(express.json());
        this.app.use('/', express.static(path.resolve(__dirname, '../public')));
        this.controller = controller;
        this.app.post('/checkLeadAuthorize',this.checkLeadAuthorize);
        this.app.post('/checkAdminAuthorize',this.checkAdminAuthorize);
        this.app.get('/getLeadsList',this.getLeadsList);
        this.app.put('/addNewLead',this.addNewLead);
        this.app.put('/updateLeadsInfo',this.updateLeadsInfo);
        this.app.delete('/deleteLeads',this.deleteLead);
        this.app.post('/getLeadComments', this.getLeadComments);
    }

    getApp = () => this.app;

    addNewLead = (req, res) => {
        const { body } = req;
        this.controller.addNewLeadObj(body);

        res.end();
    }

    deleteLead = (req, res) => {
        const { body } = req;
        this.controller.deleteLeadById(body);

        res.end()
    }

    getLeadsList = (req, res) => {
        const data = this.controller.returnLeadsList();

        res.json(data);
        res.end();
    }

    updateLeadsInfo = (req, res) => {
        const { body } = req;
        this.controller.updateLeadById(body);

        res.end();
    }

    checkLeadAuthorize = (req, res) => {
        const { body } = req;
        const authResult = this.controller.checkLeadAccountExist(body);

        res.json(authResult);
        res.end();
    }

    checkAdminAuthorize = (req, res) => {
        const { body } = req;
        const authResult = this.controller.checkAdminAuthorize(body);

        res.json(authResult);
        res.end();
    }

    getLeadComments = (req, res) => {
        const { body } = req;
        const leadCommentArray = this.controller.getLeadCommentArray(body);

        res.json(leadCommentArray);
        res.end();
    }
}

module.exports = App;